backuppc (4.4.0-10) unstable; urgency=medium

  [ Chris Hofstaedtler ]
  * Install systemd units into /usr. (Closes: #1073701)

 -- Tobias Frost <tobi@debian.org>  Sat, 27 Jul 2024 03:36:19 +0200

backuppc (4.4.0-9) unstable; urgency=medium

  * Fix setuidwrapper FTBFS with GCC-14. Closes: #1074836
  * Fix "Fails to build source after successful build", removing a generated
    file after build. Closes: #1043826
  * Add Romanian debconf translation, thanks Remus-Gabriel (Closes: #1031148)
  * Drop BackupPC_deleteFile and jLib.pm, not compatible with BackupPC V4
    (Closes: #990256)
  * Cherry-Pick patch to fix separators. (Closes: #1065055)
  * Remove patches to add pool usage graphs, as they are redundant and broken
    in BackupPC V4 (Closes: #1023757)

 -- Tobias Frost <tobi@debian.org>  Fri, 26 Jul 2024 13:58:12 +0200

backuppc (4.4.0-8) unstable; urgency=medium

  [ Axel Beckert ]
  * Update debian/watch file to follow changes on Github.
  * Add some lintian overrides for very-long-line-length-in-source-file
    false positives due to generated HTML (source included) and a long
    rrdtool command which usually gets the configuration via commandline.

  [ Andreas Hasenack ]
  * Update autopkgtest test suite to support testing authenticated SMB
    backups. (MR !3)

 -- Axel Beckert <abe@debian.org>  Sun, 15 Jan 2023 11:24:36 +0100

backuppc (4.4.0-7) unstable; urgency=medium

  * Cherry-pick upstream commit 2c9270b9 to fix configuration writing with
    Data::Dumper > 2.182 respectively Perl 5.36. (Closes: #1026256)
  * Bracketize Lintian overrides.
  * Add Lintian override for source-is-missing for doc/BackupPC.html. The
    source is doc/BackupPC.pod, the only other file in that directory.
  * Declare compliance with Debian Policy 4.6.2. (No changes needed.)
  * Prefer sysvinit-utils (>= 3.05-4~) over transitional package lsb-base.

 -- Axel Beckert <abe@debian.org>  Sun, 18 Dec 2022 13:21:37 +0100

backuppc (4.4.0-6) unstable; urgency=medium

  [ Sergio Durigan Junior ]
  * d/t/smb-backup: Provide non-empty username/password values to
    authenticate against the Samba share, working around a possible Samba
    regression with anonymous authentication. (LP: #1962166)

  [ Axel Beckert ]
  * Update copyright years in debian/copyright and actually mention the
    current Debian BackupPC team as well as the providers of the
    autopkgtest test suite. Thanks Lintian!
  * Drop -Wl,--as-needed from debian/rules. (Backports to Buster are
    unlikely now.)

 -- Axel Beckert <abe@debian.org>  Fri, 08 Apr 2022 12:19:13 +0200

backuppc (4.4.0-5) unstable; urgency=medium

  [ Simon McVittie ]
  * debian/rules: Specify canonical path to ping6. (Closes: #992647)

  [ Axel Beckert ]
  * Remove wrong declaration of /etc/backuppc/hosts being Perl syntax.
  * Refresh 09-hardcode-debians-rundir.patch to remove unwanted offset.

 -- Axel Beckert <abe@debian.org>  Thu, 02 Sep 2021 02:10:05 +0200

backuppc (4.4.0-4) unstable; urgency=medium

  * Don't try to create /var/lib/backuppc/pc/localhost/ if it already
    exists. Fixes follow-up errors if initial configuration of backuppc
    failed for some other reason. (See also #992597)
  * Drop dependencies on Perl modules which are in Perl's core since at
    least Perl 5.9.4, namely libdigest-md5-perl, libio-compress-perl, and
    libcompress-zlib-perl.
  * Declare compliance with Debian Policy 4.6.0. (No changes needed.)
  * Drop no more needed and previously redundant entry lib/systemd/system
    from debian/dirs.
  * Drop outdated sanity check in init script. (Closes: #992597).

 -- Axel Beckert <abe@debian.org>  Fri, 20 Aug 2021 20:45:09 +0200

backuppc (4.4.0-3) unstable; urgency=medium

  [ Axel Beckert ]
  * Add a note to README.Debian pointing to NEWS.Debian for upgrade
    instructions. (See discussion in #981423.)

  [ Jonathan Wiltshire ]
  * Refine debian/NEWS; split some of the detail in debian/UPGRADING

 -- Jonathan Wiltshire <jmw@debian.org>  Mon, 12 Apr 2021 19:02:10 +0100

backuppc (4.4.0-2) unstable; urgency=medium

  * postinst: Fix dpkg-statoverride warning "deprecated --force option".
  * Make backuppc-rsync a hard dependency: The daemon fails to start even
    if only SMB backups are configured. Thanks autopkgtest!
  * Add a debian/TODO list with some future/wishlist packaging todos.

 -- Axel Beckert <abe@debian.org>  Thu, 31 Dec 2020 16:54:06 +0100

backuppc (4.4.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 3.1.0-1.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Fix day-of-week for changelog entries 3.2.1-2, 2.0.0-5.

  [ Jonathan Wiltshire ]
  * New major upstream release (Closes: #873075)
    + Add new depends/recommends: libbackuppc-xs-perl,
      libfile-listing-perl, backuppc-rsync, libfile-rsyncp-perl
    + Refresh patches for 4.3.1, debian/{rules,dirs,docs}.
    + Install upstream systemd service file.
    + Add patch 08-fix-systemd-unit.patch: use Debian-ish concepts in the
      service file.
    + Add a debian/NEWS entry wrt. to necessary config changes and
      installation of the backuppc-rsync package on clients.
    [ Axel Beckert ]
    + 07-reload-user.patch: Update variable names and path to upstream
      init script.
    + Refresh patches for 4.4.0 including debian/config.pl.diff, drop
      hunks from 01-debian.patch which were applied upstream.
    + Bump minimum versions of backuppc-rsync and libbackuppc-xs-perl
      according to the 4.4.0 upstream release notes.
    + Suggest libscgi-perl.
    + Drop libfile-rsyncp-perl from Recommends. It has been replaced with
      backuppc-rsync. This also solves issues caused by File::RsyncP like
      missing rsync compression support. (Closes: #538272, #954241)
    + Update init script to be closer to upstream's init script again.
      - Update documentation.
      - Use $RUNDIR and define it once.
    + Update debian/README.Debian and debian/NEWS with regards to what's
      needed for BackupPC 4 and especially the migration from V3 to V4.
    + Add patch to hardcode /run/backuppc as RunDir in BackupPC::Lib.
    + Add some of the new dependencies also as build-dependencies.
    + Drop dependency on libwww-perl, replaced by dependency on
      libfile-listing-perl. (See #602787)
    + Demote rsync to suggestion, now only needed for backing up localhost
      using rsync, replaced by backuppc-rsync on the server-side.
    + Drop 03-ipv6.patch and libsocket6-perl dependencies. (BackupPC 4 has
      native IPv6 support albeit implemented differently.)

  [ Axel Beckert ]
  * Rename debian/{NEWS.Debian → NEWS}, fix syntax of 2018 trailer line.
  * Set "Rules-Requires-Root: binary-targets".
  * Add lintian override for executable-in-usr-lib triggered by CGI
    script.
  * Declare compliance with Debian Policy 4.5.1. (No changes needed.)
  * Recode debian/po/{fr,nl}.po from ISO-8859-15 to UTF-8. Fixes lintian
    warning national-encoding.
  * Bump debhelper-compat to 13.
  * Add a comment to document when to remove the -Wl,--as-needed linker
    flag. (Lintian warning debian-rules-uses-as-needed-linker-flag)
  * Extend and improve long package description.
  * Extend 02-fix-spelling-errors.patch to fix more typos.

 -- Axel Beckert <abe@debian.org>  Wed, 30 Dec 2020 17:20:09 +0100

backuppc (3.3.2-3) unstable; urgency=medium

  * Update watch file for BackupPC 4 series
  * 07-reload-user.patch: pass the username to start-stop-daemon when
    reloading (Closes: #944611)
  * Add me to uploaders

 -- Jonathan Wiltshire <jmw@debian.org>  Tue, 12 Nov 2019 17:57:56 +0000

backuppc (3.3.2-2) unstable; urgency=low

  * There are no logs to chown upon a first installation, so don't try to
    chown them. (Fixes issue found by piuparts.)
  * Avoid recursive chown to /etc/backuppc, just chown /etc/backuppc and
    all files in it. By default it has no subdirectories.
  * Update line numbers in debian/config.pl.diff.

 -- Axel Beckert <abe@debian.org>  Wed, 14 Nov 2018 23:27:21 +0100

backuppc (3.3.2-1) unstable; urgency=low

  [ Axel Beckert ]
  * Set Maintainer to the new Debian BackupPC team. (Closes: #887490)
    + Add Tobias Frost and myself to Uploaders.
  * Import new 3.3.2 upstream release.
    + Drop some hunks from 01-debian.patch which upstream applied or fixed
      otherwise:
      - lib/BackupPC/CGI/Browse.pm
      - all hunks which fix "{" to "\{" in regular expressions.
    + Refresh the remaining part of 01-debian.patch and fix non-applying
      hunks manually.
    + Remove unnecessary whitesapce changes from 01-debian.patch.
    + 01-debian.patch: Continue to use UTF-8 in doc/BackupPC.pod despite
      upstream has chose to explicitly declare it as ISO8859-1.
    + Refresh 02-fix-spelling-errors.patch.
  * Ship https://sourceforge.net/p/backuppc/mailman/message/27416276/ as
    debian/jLib.pm and install as BackupPC::jLib. (Closes: #876190)
    Thanks: Martin F Krafft and Alex Vandiver.
  * Run "wrap-and-sort -a".
  * Support IPv6 by extracting the according patch hunks from Ubuntu's
    backuppc package. (Closes: #876233, #610932)
    + Expand patch with web configuration editor support by lynxis
      lazus. (LP: #1349031)
  * Add lintian overrides for these false positives:
    + spelling-error-in-readme-debian (User and group name are the same
      and trigger a "duplicate word" case).
    + unused-debconf-template (mentioned template is used)
  * Fix typo in 3.3.1-4 changelog entry.
  * Add patch to replace the rather unlucky "IS" with "IT support" in mail
    templates. (Closes: #808241)
  * Add patch to fix bad e-mail headers in Polish and Czech mail templates
    as well as the encoding of the Czech mail templates. (Closes: #675013)
    Thanks Vladislav Kurz for the bug report!
  * debian/apache.conf:
    + Switch to Apache 2.4 syntax.
    + Only grant access from localhost by default (as by default HTTPS is
      not enforced). Mention that in NEWS.Debian.
    + Fix typo in comment.
  * Suggest a bunch of letsencrypt clients with certbot as default.
  * Document conf/sorttable.js in debian/copyright.
  * Unconditionally use invoke-rc.d in post{inst,rm}, drop existence check
  * Consistently use debian/<name> instead of debian/backuppc.<name> and
    not half/half.
    + Rename debian/examples directory to debian/example-configs to avoid
      name conflict. Update debian/examples file accordingly.
  * debian/config: Use "set -e" instead of "-e" in the shebang line.
  * debian/post{inst,rm}: Add "set -e" at the beginning.
  * Apply patch to run rrdtool as the backuppc user rather than as the
    www-data user to fix the displaying of the RRD graphs in the web
    interface. (Closes: #903217, LP: #1612600)

  [ Andreas Hasenack ]
  * d/t/{control,smb-backup}: simple smb-based DEP8 test (LP: #1677755)

 -- Axel Beckert <abe@debian.org>  Wed, 14 Nov 2018 04:07:00 +0100

backuppc (3.3.1-6) unstable; urgency=medium

  * QA upload.
  * Imported package to salsa.debian.org and set VCS-* fields accordingly.
  * Execute wrap-and-sort.
  * Convert to 3.0 (quilt) package format. The changes to the upstream
    sources have been put into 01-debian.patch.
  * Fix "FTBFS with merged /usr" by specifying all binary paths and not letting
    configure.pl letting them guess them. (Closes: #905761)
  * Modernize Package:
    - compat level 11 (d/compat and add B-D on debhelper >=11)
    - migrate to short debhelper.
    - drop the versioned conflict against libfile-rsyncp-perl, version
      in old-old-stable is already new enough.
    - remove versions specifier from dependencies, all are fulfilled in
      old-old-stable already. Tar and dpkg can be dropped, they are
      essential.
    - drop d/menu.
    - convert d/copyright to the Machine-readable copyright file format.
    - modify d/watch to point to (new) github project site.
    - S-V bumped to 4.2.1:  required.
    - postinst: do not check existence of executables with absolute path,
      but using which. (lintian command-with-path-in-maintainer-script).
  * Add patch to fix some spelling errors found by lintian.
  * Fix "Wrong path in BackupPC_deleteFile", patch from madduck (Closes:
    #876188)
  * Fix "BackupPC_deleteFile uses deprecated defined @{array}", patch from
    madduck. (Closes: #876193)
  * Do only recommmend smbclient and samba-common-bin, do not depend on it
    (Closes: #488098, #820980)
  * Change homepage in d/control to new upstream location.

 -- Tobias Frost <tobi@debian.org>  Wed, 17 Oct 2018 23:53:54 +0200

backuppc (3.3.1-5) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group. (see #887490)
  * Add the missing dependency on lsb-base.
  * Add Homepage field.

 -- Adrian Bunk <bunk@debian.org>  Fri, 16 Feb 2018 16:48:37 +0200

backuppc (3.3.1-4) unstable; urgency=low

  * Added dependency on libcgi-pm-perl. Closes: #820092
  * Fixed unescaped left brace in regex. Closes: #820110
  * Remove /etc/backuppc/htpasswd /etc/backuppc/pc on purge.
    Closes: #810691

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 30 Nov 2016 19:57:51 +0100

backuppc (3.3.1-3) unstable; urgency=medium

  * Regexps fix for smbclient >= 4.2 to avoid failing SMB backups
    And set $Conf{BackupZeroFilesIsFatal} = 0 in the default config.pl
    Closes: #820963
  * Workaround for SMB restores: '-d' 5 is now the default for smbclient
  * Added some cleanup regexps for SMB backup logs

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 23 May 2016 14:11:28 +0200

backuppc (3.3.1-2) unstable; urgency=medium

  * Perl 5.22 fix. Closes: #809626
  * Only remove /etc/backuppc/config.pl on purge. Closes: #800001
  * Added BackupPC_deleteFile. Closes: #806671

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 05 Jan 2016 20:48:20 +0100

backuppc (3.3.1-1) unstable; urgency=low

  * New upstream release
  * Now depends on libtime-parsedate-perl. Closes: #749879
  * Added support for IONICE in startup script. Closes: #772149

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 10 Aug 2015 18:10:59 +0200

backuppc (3.3.0-2) unstable; urgency=low

  * Depends on apache2-utils
  * removed a superfluous libtime-modules-perl. Closes: #734356
  * Typo in log message fixed. Closes: #731087
  * using dpkg-buildflags

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 06 Feb 2014 12:41:22 +0100

backuppc (3.3.0-1) unstable; urgency=low

  * New upstream release. Closes: #716824
  * Fix Typo in kill signal name (ALRM vs ARLM). Closes: #698441
  * Enable Apache2 (2.4) configuration by default in debconf.

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 12 Sep 2013 6:13:50 +0200

backuppc (3.2.1-5.2) unstable; urgency=low

  * Non-maintainer upload.
  * Patch BackupPC.pod for POD errors with Perl 5.18
    pod2man has become more strict with perl 5.18. The applied patch
    converts the non-7-bit clean character into UTF-8 and declares the
    file's encoding as such.
    Thanks to Dominic Hargreaves <dom@earth.li> (Closes: #719827)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 24 Aug 2013 15:29:57 +0200

backuppc (3.2.1-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Do not ship /etc/backuppc/config.pl as a conffile; it is handled
    by ucf already (Closes: #706315)

 -- Jonathan Wiltshire <jmw@tiger-computing.co.uk>  Fri, 07 Jun 2013 11:42:08 +0100

backuppc (3.2.1-5) unstable; urgency=low

  * Added libtime-modules-perl dependency. Closes: #525395
  * Remove /var/lib/backuppc/pc/localhost if it's empty
    Closes: #672372
  * Added stuff to support Apache 2.4. Closes: #669765
  * Added NICE support in /etc/default/backuppc. Closes: #639102
  * Changed the default display date format. Closes: #663975

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 03 Jan 2013 17:27:35 +0100

backuppc (3.2.1-4) unstable; urgency=low

  * init.d status support. Closes: #651539
  * fixed a qw() bug. Closes: #650522

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 20 Dec 2012 17:59:28 +0100

backuppc (3.2.1-3) unstable; urgency=high

  * urgency set to high because of a security fix
  * fixed the XSS in CGI/RestoreFile.pm. CVE-2011-5081. Closes: #673331
  * added the Polish debconf translation. Closes: #673698
  * updated Danish translation. Closes: #659456
  * added autofs in init.d's Should-Start/Should-Stop. Closes: #662908

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 31 May 2012 11:51:16 +0200

backuppc (3.2.1-2) unstable; urgency=high

  * Really fix CVE-2011-3361. Closes: #646865

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 02 Nov 2011 11:03:38 +0100

backuppc (3.2.1-1) unstable; urgency=high

  * New upstream release. Closes: #641450
  * urgency set to high because of a security fix

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 13 Sep 2011 20:38:00 +0200

backuppc (3.2.0-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use inetutils-ping as an alternative to iputils-ping. Closes: #630777

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 28 Jun 2011 22:31:10 +0200

backuppc (3.2.0-4) unstable; urgency=low

  * debian/setuidwrapper.c: New.
  * debian/rules: Build setuidwrapper as cgi-bin/index.cgi; install real
    index.cgi as lib/realindex.cgi.  Closes: #581950.
  * control: Remove depends on perl-suid.  Change to Architecture any, add
    ${shlibs:Depends}.

 -- Ludovic Drolez <ldrolez@debian.org>  Sat, 07 May 2011 10:11:56 -0500

backuppc (3.2.0-3) unstable; urgency=low

  * Shell characters are now properly escaped. Closes: #516626
  * Added in README.Debian instructions for IPv6 hosts. Closes: #594091
  * Removed /etc/backuppc/htgroup. Closes: #594692

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 10 Jan 2011 12:29:07 +0100

backuppc (3.2.0-2) unstable; urgency=low

  * re-added the xfs/rfs pool size patch. Closes: #600654
  * updated the pt_BR debconf translation. Closes: #607405

 -- Ludovic Drolez <ldrolez@debian.org>  Sun, 02 Jan 2011 19:33:07 +0100

backuppc (3.2.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add depends on libwww-perl (Closes: #602787)

 -- Patrick Winnertz <winnie@debian.org>  Fri, 26 Nov 2010 14:37:21 +0100

backuppc (3.2.0-1) unstable; urgency=low

  * New upstream release

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 06 Oct 2010 20:57:45 +0200

backuppc (3.1.0-10) unstable; urgency=low

  * Dependenct added: libtime-modules-perl. Closes: #570843
  * Removed useless logging. Closes: #558431

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 12 Apr 2010 20:30:28 +0200

backuppc (3.1.0-9) unstable; urgency=low

  * chown/chmod only the needed dirs. Closes: #531948
  * added 'Allow from all' in apache conf. Closes: #538976
  * depends on samba-common-bin. Closes: #546660
  * depends on default-mta. Closes: #539796
  * added the xfs/rfs pool size patch. Closes: #556729 #544852
  * specified a font name for graphs. Closes: #544853
  * updated debconf translations. Closes: #526699
  * added a nice value in the init. Closes: #438614
  * only keep apache2 auto-config. Closes: #528965

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 24 Nov 2009 10:34:10 +0100

backuppc (3.1.0-8) unstable; urgency=high

  * Really fix the alias bug. Closes: #542218
  * Small init.d file fix

 -- Ludovic Drolez <ldrolez@debian.org>  Fri, 09 Oct 2009 20:58:32 +0200

backuppc (3.1.0-7) unstable; urgency=high

  * Disable the modification of the alias for normal users. Closes: #542218
  * Recommends: libio-dirent-perl. Closes: #518554
  * manage config.pl with ucf. Closes: #483573

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 01 Sep 2009 14:43:36 +0200

backuppc (3.1.0-6) unstable; urgency=high

  * Fix the permissions of the CGI script. Closes: #518518
  * Fix the permissions of htpasswd/htgroup files
  * Enabled MD5 hash for htpasswd by default

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 31 Mar 2009 11:30:48 +0200

backuppc (3.1.0-5) unstable; urgency=low

  * MTA dependency added. Closes: #458961
  * iputils-ping dependency added. Closes: #516147
  * remove the /etc/$webserver/conf.d/backuppc link if present. Closes: #516159
  * Small debian/rules fix. Closes: #488659
  * Apache1 dep removed. Closes: #488656
  * /etc/backuppc/localconfig.pl can be used for customizations to config.pl.
    Closes: #375998
  * Updated debconf translations. Closes: #513665, #503741
  * Enable basic auth apache2 modules

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 03 Mar 2009 21:29:55 +0100

backuppc (3.1.0-4) unstable; urgency=medium

  * Failing SMB backups: removed the '-N' option for smbclient because
    of an undocumented change in samba. See bug #501057
  * Updated Sweedish debconf file. Closes: #491937
  * Added Russian debconf file. Closes: #501756
  * Added Finnish debconf file. Closes: #501934
  * Added Ukrainian debconf file. Closes: #501977
  * Added Basque debconf file. Closes: #502076
  * Added Italian debconf file. Closes: #502463
  * Changed the doc-base section to `File Management'
  * Documented some known bugs with WAs.

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 07 Oct 2008 20:10:20 +0200

backuppc (3.1.0-3) unstable; urgency=low

  * Added backuppc to /etc/aliases. Closes: #438331
  * Moved the pool size graphs to the bottom. Closes: #476659
  * Watch file fixed

 -- Ludovic Drolez <ldrolez@debian.org>  Tue,  8 Apr 2008 12:26:57 +0200

backuppc (3.1.0-2) unstable; urgency=low

  * patched backuppc to show a nice rrdtool graph of the pool size.
  * added a conflicts: libfile-rsyncp-perl (< 0.68). Closes: #431979
  * removed the forgotten wwwconfig-common from dependencies
  * Japanese debconf template updated. Closes: #463646
  * Added the SSL option in apache.conf. Closes: #437685

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 21 Feb 2008 20:49:07 +0100

backuppc (3.1.0-1) unstable; urgency=low

  * New upstream release. Closes: #437451
  * Moved MTAs, rsync and openssh to the recommends line. Smbclient is still
    needed. Closes: #454795, #441337
  * symlink /etc/backuppc/pc to /etc/backuppc to follow the doc. Closes: #436793
  * moved the creation of /var/lib/backuppc/pc/localhost to the postinst.
    Closes: #451922
  * Added the PT debconf translation. Closes: #434291
  * Unfuzzy config.pl.diff. Closes: #452673
  * Support /var/run on tmpfs and use LSB logging. Closes: #452672
  * Properly remove /etc/apache*/conf.d/backuppc.conf. Closes: #452677

 -- Ludovic Drolez <ldrolez@debian.org>  Fri,  7 Dec 2007 10:54:44 +0100

backuppc (3.0.0-4) unstable; urgency=high

  * Clear the remaining password in config.dat. Closes: #436681

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 27 Aug 2007 18:28:25 +0200

backuppc (3.0.0-3) unstable; urgency=medium

  * Added bzip2 in the dependencies. Closes: #420030
  * Added a LSB section in the init script.

 -- Ludovic Drolez <ldrolez@debian.org>  Sun, 20 May 2007 16:38:13 +0200

backuppc (3.0.0-2.1) unstable; urgency=high

  * Non-maintainer upload during BSP.
  * Fix non-conditional use of debconf during purge (Closes: #416650).
  * Updated German translation, thanks Erik Schanze (Closes: #412020).
  * Updated Czech translation, thanks Martin Sin (Closes: #412729).

 -- Luk Claes <luk@debian.org>  Thu, 17 May 2007 00:37:56 +0200

backuppc (3.0.0-2) unstable; urgency=low

  * Bzip2 path changed. Closes: #410858
  * Refuse to run the daemon if an old config.pl file is detected.

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 14 Feb 2007 8:27:12 +0100

backuppc (3.0.0-1) unstable; urgency=low

  * New upstream release. Closes: #369684, #377084, #406681, #405822, #408916
  * /etc/backuppc is now owned by user backuppc, so that the CGI can modify the
    configuration.
  * README.Debian updated. Closes: #407467
  * Updated README.Debian with information about the backuppc account, and
    about installation with Apache 2.2.x. Closes: #390798
  * Do not use wwwconfig-common anymore. Closes: #304901, #249010

 -- Ludovic Drolez <ldrolez@debian.org>  Mon,  5 Feb 2007 10:12:51 +0100

backuppc (2.1.2-5) unstable; urgency=medium

  * upstream patch 2.1.2pl1 applied. Closes: #369425, #355112
  * added -D to rsync flags. Closes: #365437, #358319
  * localhost incremental backups fixed. Closes: #369465
  * suggests openssh-client or ssh-client. Closes: #369464
  * paths in the documentation fixed. Closes: #369428

 -- Ludovic Drolez <ldrolez@debian.org>  Wed,  7 Jun 2006 10:00:31 +0200

backuppc (2.1.2-4) unstable; urgency=low

  * Added BackupPC-2.1.2pl0.diff. Closes: #347930

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 23 Jan 2006 21:43:30 +0100

backuppc (2.1.2-3) unstable; urgency=low

  * Added Spanish and Swedish debconf translations. Closes: #333877, #330940
  * Avoid the creation of etc/config.pl.orig
  * DH_COMPAT=4

 -- Ludovic Drolez <ldrolez@debian.org>  Sun,  8 Jan 2006 23:52:56 +0100

backuppc (2.1.2-2) unstable; urgency=low

  * Added misc:Depends in control file. Closes: #331756.

 -- Ludovic Drolez <ldrolez@debian.org>  Sun, 13 Nov 2005 13:40:15 +0100

backuppc (2.1.2-1) unstable; urgency=low

  * New upstream release. Closes: #327125, #317714.

 -- Ludovic Drolez <ldrolez@debian.org>  Thu,  8 Sep 2005 18:25:22 +0200

backuppc (2.1.1-4) unstable; urgency=medium

  * Fixed an important bug in BackupPC_tarCreate which can cause archives to
    be corrupted.
  * Put pid file in /var/run/backuppc/BackupPC.pid, Closes: #317748
  * Check if ssh and par2 exists. Closes: #319246
  * Now depends on a MTA.

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 22 Aug 2005 18:55:15 +0200

backuppc (2.1.1-3) unstable; urgency=low

  * Typo in french translation removed. Closes: #305475
  * Added Vietnamese debconf translation. Closes: #309327
  * Added Czech debconf translation. Closes: #304877
  * Added instructions in README.Debian on installing the CGI under
    suexec+apache2 (contributed by William McKee). Closes: #309700

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 28 Jun 2005 13:02:06 +0200

backuppc (2.1.1-2) unstable; urgency=low

  * Added a patch to have backuppc detach correclty. Closes: #301057
  * Hostname fix. Closes: #301360
  * Small CGI fix: the host config file link was not present.

 -- Ludovic Drolez <ldrolez@debian.org>  Fri,  1 Apr 2005 22:07:03 +0200

backuppc (2.1.1-1) unstable; urgency=low

  * New upstream release
  * Do not modify config.pl in the postinst.
    Thanks to Peter Palfrader for the patch. Closes: #301076.
  * Moved the 'rm -rf /etc/backuppc'. Closes: #301084.
  * Check that backuppc.config is not run 2 times before generating
    a random password. Closes: #301087.
  * Added /bin/sh as shell for the backuppc user. Closes: #299852.

 -- Ludovic Drolez <ldrolez@debian.org>  Fri, 18 Mar 2005 20:25:47 +0100

backuppc (2.1.0-10) unstable; urgency=medium

  * Explicitly run /usr/bin/env : do not rely on the PATH.
    Closes: #297879.
    Priority set to medium since it could break existing setups.
  * Removed the debconf templates which should not be translated.
  * Added pt_BR debconf translation. Closes: #297052

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 28 Feb 2005 18:56:02 +0100

backuppc (2.1.0-9) unstable; urgency=low

  * A random password is generated for backuppc http user. Closes: #293848
  * $Conf{CgiURL} corrected in configure.pl. Closes: #287281
  * Added 'env LC_ALL=C' before tar in config.pl to prevent locale problems
  * added the tarssh-sudo.pl example configuration file.
  * postrm script fixed. Closes: #293847
  * config comment fixed. Closes: #279041

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 28 Dec 2004 15:38:06 +0100

backuppc (2.1.0-8) unstable; urgency=low

  * Postinst script fixed. Closes: Bug#283755

 -- Ludovic Drolez <ldrolez@debian.org>  Sat,  4 Dec 2004 01:54:02 +0100

backuppc (2.1.0-7) unstable; urgency=low

  * Depends on libfile-rsyncp-perl >= 0.50. Closes: #279649

 -- Ludovic Drolez <ldrolez@debian.org>  Fri,  5 Nov 2004 14:03:38 +0100

backuppc (2.1.0-6) unstable; urgency=medium

  * Moved 'db_stop' after 'db_purge'. Closes: #276306
  * Urgency=medium because this important bug was fixed.
  * Po-debconf translations updated. Closes: #270786, #271100, #274656

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 21 Oct 2004 23:43:18 +0200

backuppc (2.1.0-5) unstable; urgency=low

  * Added the pl1 patch. Closes: #270230
  * Postint modified to use a2enmod. Closes: #269434

 -- Ludovic Drolez <ldrolez@debian.org>  Thu,  9 Sep 2004 08:56:10 +0200

backuppc (2.1.0-4) unstable; urgency=medium

  * Backuppc now asks if it can modify the apache config. Closes: #268068
  * Urgency=medium because an RC bug has been fixed.
  * fr.po updated.

 -- Ludovic Drolez <ldrolez@debian.org>  Tue, 31 Aug 2004 20:19:55 +0200

backuppc (2.1.0-3) unstable; urgency=medium

  * Added a patch to make the archive feature working. Closes: Bug#263409
  * Urgency=medium because this upload fixes a new feature of backuppc 2.1
  * User backuppc now created as a system user since it's mainly used
    to run the daemon. Closes: Bug#255498

 -- Ludovic Drolez <ldrolez@debian.org>  Thu,  5 Aug 2004 20:53:37 +0200

backuppc (2.1.0-2) unstable; urgency=low

  * Removed a slash in the apache alias. Closes: Bug#261108
  * Removed a few checks in the configure script. Closes: Bug#261041
  * added a 'dpkg-statoverride --remove' in the postrm script

 -- Ludovic Drolez <ldrolez@debian.org>  Fri, 23 Jul 2004 22:49:11 +0200

backuppc (2.1.0-1) unstable; urgency=low

  * New upstream release. Closes: #255482
  * Added in README.Debian what additional packages you need for
    rsync based backups. Closes: #257518
  * suggests exim4. Closes: #255338

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 15 Jul 2004 09:30:12 +0200

backuppc (2.0.2-6) unstable; urgency=low

  * German debconf translation added. Closes: Bug#250573
  * Dependencies: added apache-ssl | apache-perl

 -- Ludovic Drolez <ldrolez@debian.org>  Thu,  3 Jun 2004 09:36:22 +0200

backuppc (2.0.2-5) unstable; urgency=low

  * XFerLog filter patch applied. Closes: Bug#248604

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 12 May 2004 20:05:55 +0200

backuppc (2.0.2-4) unstable; urgency=low

  * solved the doc substitution problem. Closes: Bug#237322
  * nl.po debconf translation added. Closes: Bug#241308
  * quiet ssh flag added in $Conf{RsyncClientCmd}. Closes: Bug#242621

 -- Ludovic Drolez <ldrolez@debian.org>  Sat, 10 Apr 2004 00:08:24 +0200

backuppc (2.0.2-3) unstable; urgency=low

  * apache2 support. Closes: Bug#214548.
  * Japanese po-debconf template translation added. Closes: Bug#227229

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 15 Mar 2004 23:11:45 +0100

backuppc (2.0.2-2) unstable; urgency=low

  * Docbase path fixed. Closes: Bug#224250
  * More copyright info
  * Added a mysql backup file example.

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 17 Dec 2003 20:02:35 +0100

backuppc (2.0.2-1) unstable; urgency=low

  * New upstream release
  * debian/watch added
  * chown syntax fixed. Closes: #211987
  * chown/chmod called only on the 1st install. Closes: #214526

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 16 Oct 2003 21:56:25 +0200

backuppc (2.0.0-5) unstable; urgency=low

  * Added gettext based debconf templates. Closes: Bug#205787
  * Added fr.po debconf file. Closes: Bug#206592
  * Added some basic examples
  * Fixed the default 'hosts' file: 'localhost' is now owned by 'backuppc'
  * Fixed BackupPC_Admin to be able to display config files in /etc/backuppc
  * Updated README.debian file. Closes: #203234, #203272

 -- Ludovic Drolez <ldrolez@debian.org>  Sat, 23 Aug 2003 21:34:44 +0200

backuppc (2.0.0-4) unstable; urgency=low

  * set Architecture to all
  * depends: samba-tng-common or samba-common
  * Menu file added. Closes: #201148
  * improved description. Closes: #199170

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 26 Jun 2003 21:21:21 +0200

backuppc (2.0.0-3) unstable; urgency=low

  * added --gecos "" to the adduser line on the postinst. Closes: #198814
  * init script small fix

 -- Ludovic Drolez <ldrolez@debian.org>  Thu, 26 Jun 2003 20:57:53 +0200

backuppc (2.0.0-2) unstable; urgency=low

  * added dependencies: libdigest-md5-perl
  * packaged libfile-rsyncp-perl: you can now use rsync for backups
  * images path fixed
  * documentation path fixed in cgi script

 -- Ludovic Drolez <ldrolez@debian.org>  Wed, 18 Jun 2003 22:08:19 +0200

backuppc (2.0.0-1) unstable; urgency=low

  * Initial Release. Closes: Bug#158874.
  * modified configure.pl for non-interactive install
  * modified perl scripts for having main config files in /etc/backuppc

 -- Ludovic Drolez <ldrolez@debian.org>  Mon, 16 Jun 2003 10:43:48 +0200
